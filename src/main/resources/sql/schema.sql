CREATE TABLE IF NOT EXISTS employee (
    id BIGINT AUTO_INCREMENT NOT NULL,
    firstName VARCHAR(255) NOT NULL,
    lastName VARCHAR(255) NOT NULL,
    employeeType VARCHAR(255) NOT NULL,
    isActive BIT DEFAULT 1,
    PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS timeRecord (
    id BIGINT AUTO_INCREMENT NOT NULL,
    employeeId BIGINT NOT NULL,
    timeIn TIME NOT NULL,
    timeOut TIME NOT NULL,
    isUpdated BIT DEFAULT 1,
    date DATE NOT NULL,
    PRIMARY KEY (id),
    FOREIGN KEY (employeeId) REFERENCES employee(id)
    ON UPDATE CASCADE

);
CREATE TABLE IF NOT EXISTS leaveRecord (
    id BIGINT AUTO_INCREMENT NOT NULL,
    employeeId BIGINT NOT NULL,
    leaveType VARCHAR(255) NOT NULL,
    date DATE NOT NULL,
    isApproved BIT DEFAULT 1,
    PRIMARY KEY (id),
    FOREIGN KEY (employeeId) REFERENCES employee(id)
    ON UPDATE CASCADE
);

