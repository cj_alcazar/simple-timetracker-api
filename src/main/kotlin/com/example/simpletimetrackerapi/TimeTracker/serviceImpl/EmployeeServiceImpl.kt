package com.example.simpletimetrackerapi.TimeTracker.serviceImpl

import com.example.simpletimetrackerapi.TimeTracker.entities.Employee
import com.example.simpletimetrackerapi.TimeTracker.repositories.EmployeeRepository
import com.example.simpletimetrackerapi.TimeTracker.service.EmployeeService
import com.example.simpletimetrackerapi.TimeTracker.utils.EmployeeType
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Service
import org.springframework.web.server.ResponseStatusException

@Service
class EmployeeServiceImpl (
    private val repository: EmployeeRepository): EmployeeService {
    override fun createEmployee(body: Employee): Employee {

        if (repository.doesfirstNameandLastnameExist(body.firstName,body.lastName))
            throw ResponseStatusException(HttpStatus.CONFLICT,
                "Firstname and lastname ${body.firstName} ${body.lastName} already exist.")

        val employeeType = EmployeeType.valueOf(body.employeeType)


        return repository.save(body.copy(
            employeeType = employeeType.name
        ))
    }

    override fun getEmployeeById(id: Long): Employee {
        val employee = repository.findById(id).orElseThrow{
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id does not exist.")
        }
        return employee
    }

    override fun updateEmployee(body: Employee, id: Long): Employee {
        val employee = repository.findById(id).orElseThrow{
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id does not exist.")
        }
        if (employee.employeeType != EmployeeType.valueOf(body.employeeType).name)
            throw ResponseStatusException(HttpStatus.CONFLICT, "Sorry, you are not allowed to change the employeeType.")

        return repository.save(employee.copy(
            firstName = body.firstName,
            lastName = body.lastName
        ))
    }


    override fun deleteEmployee(id: Long): Employee {
        val employee = repository.findById(id).orElseThrow{
            ResponseStatusException(HttpStatus.NOT_FOUND, "Employee with id: $id does not exist.")
        }
        if (employee.isActive) return repository.save(employee.copy(isActive = false))
        else return repository.save(employee.copy(isActive = true))

    }

    override fun getAllEmployee(): List<Employee> {
        return repository.findAll().toList()
    }
}