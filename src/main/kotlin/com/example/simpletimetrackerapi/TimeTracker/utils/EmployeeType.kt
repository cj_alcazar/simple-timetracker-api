package com.example.simpletimetrackerapi.TimeTracker.utils

enum class EmployeeType (val value: String) {
    ProjectManager("Project Manager"),
    Contractor("Contractor")

}