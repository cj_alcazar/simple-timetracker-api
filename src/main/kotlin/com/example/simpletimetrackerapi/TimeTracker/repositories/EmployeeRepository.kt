package com.example.simpletimetrackerapi.TimeTracker.repositories

import com.example.simpletimetrackerapi.TimeTracker.entities.Employee
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface EmployeeRepository: CrudRepository<Employee, Long> {
    @Query(
        """
           SELECT CASE WHEN COUNT(e) > 0 THEN TRUE ELSE FALSE END
           FROM Employee e WHERE e.firstName = :firstName AND e.lastName =:lastName
        """
    )
    fun doesfirstNameandLastnameExist(firstName: String, lastName: String): Boolean

}