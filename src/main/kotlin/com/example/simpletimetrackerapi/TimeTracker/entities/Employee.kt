package com.example.simpletimetrackerapi.TimeTracker.entities

import javax.persistence.*

@Entity(name = "Employee")
@Table(name = "employee")
data class Employee(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long? = null, // Long ->BIGINT

    @Column(
        nullable = false,
        updatable = true,
        name = "firstName"
    )
    var firstName: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "lastName"
    )
    var lastName: String,

    @Column(
        nullable = false,
        updatable = false,
        name = "employeeType"
    )
    var employeeType: String,

    @Column(
        nullable = false,
        updatable = true,
        name = "isActive"
    )
    var isActive: Boolean = true,

)
