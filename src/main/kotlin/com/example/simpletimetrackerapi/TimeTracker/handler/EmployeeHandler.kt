package com.example.simpletimetrackerapi.TimeTracker.handler

import com.example.simpletimetrackerapi.TimeTracker.entities.Employee
import com.example.simpletimetrackerapi.TimeTracker.service.EmployeeService
import org.springframework.web.bind.annotation.*
@RestController
class EmployeeHandler(
    private val employeeService: EmployeeService
) {
    @PostMapping("/api/employee/")
    fun createEmployee(@RequestBody employee: Employee): Employee = employeeService.createEmployee(employee)

    @GetMapping("/api/employee/{id}/")
    fun getEmployeeById(@PathVariable("id") id: Long): Employee = employeeService.getEmployeeById(id)

    @PutMapping("/api/employee/{id}/")
    fun updateUser(@RequestBody body: Employee, @PathVariable("id") id: Long): Employee =
        employeeService.updateEmployee(body, id)

    @PutMapping("/api/employeeChange/{id}/")
    fun deleteEmployee(@PathVariable("id") id: Long) = employeeService.deleteEmployee(id)

    @GetMapping("/api/employees/")
    fun getAllUsers(): List<Employee> = employeeService.getAllEmployee()
}