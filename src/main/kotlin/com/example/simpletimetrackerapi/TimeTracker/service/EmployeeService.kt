package com.example.simpletimetrackerapi.TimeTracker.service

import com.example.simpletimetrackerapi.TimeTracker.entities.Employee

interface EmployeeService {
    fun createEmployee(body: Employee): Employee
    fun getEmployeeById(id: Long): Employee
    fun updateEmployee(body: Employee, id: Long): Employee
    fun deleteEmployee(id: Long) : Employee
    fun getAllEmployee(): List<Employee>
}