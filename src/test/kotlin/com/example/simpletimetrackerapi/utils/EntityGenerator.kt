package com.example.simpletimetrackerapi.utils

import com.example.simpletimetrackerapi.TimeTracker.entities.Employee
import com.example.simpletimetrackerapi.TimeTracker.utils.EmployeeType

object EntityGenerator {
    fun createEmployee(): Employee = Employee(
        firstName = "Brandon",
        lastName = "Cruz",
        employeeType = EmployeeType.ProjectManager.name
    )

}