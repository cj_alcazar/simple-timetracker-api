package com.example.simpletimetrackerapi.timeRecord

import com.example.simpletimetrackerapi.SimpleTimetrackerApiApplicationTests
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.test.web.servlet.MockMvc
@AutoConfigureMockMvc
class TimeRecordServiceIntegrationTest : SimpleTimetrackerApiApplicationTests() {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper

}