package com.example.simpletimetrackerapi.employee

import com.example.simpletimetrackerapi.TimeTracker.repositories.EmployeeRepository
import com.example.simpletimetrackerapi.TimeTracker.service.EmployeeService
import com.example.simpletimetrackerapi.SimpleTimetrackerApiApplicationTests
import com.example.simpletimetrackerapi.TimeTracker.serviceImpl.EmployeeServiceImpl
import com.example.simpletimetrackerapi.TimeTracker.utils.EmployeeType
import com.example.simpletimetrackerapi.utils.EntityGenerator
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertFailsWith

class EmployeeServiceImplTest: SimpleTimetrackerApiApplicationTests() {
    @Autowired
    private lateinit var employeeRepository: EmployeeRepository

    @Autowired
    private lateinit var employeeServiceImpl: EmployeeServiceImpl

    @BeforeEach
    fun setUp(){
        employeeRepository.deleteAll()
    }

    @Test
    fun `create should return employee`(){
        assertEquals(0, employeeRepository.findAll().count())
        val employee = EntityGenerator.createEmployee()
        val createdUser = employeeServiceImpl.createEmployee(employee)
        assertEquals(1, employeeRepository.findAll().count())
        assertEquals(employee.firstName, createdUser.firstName)
        assertEquals(employee.lastName, createdUser.lastName)
        assertEquals(employee.employeeType, createdUser.employeeType)
    }
    @Test
    fun `create should fail given duplicate firstname and lastname`(){
        assertEquals(0, employeeRepository.findAll().count())

        val employee = EntityGenerator.createEmployee()
        employeeRepository.save(employee)

        val duplicateUser = employee.copy(
            firstName = "Brandon",
            lastName = "Cruz",
        )
        val exception = assertFailsWith<ResponseStatusException> {
            employeeServiceImpl.createEmployee(duplicateUser)
        }
        val expectedException =
            "409 CONFLICT \"Firstname and lastname ${duplicateUser.firstName} ${duplicateUser.lastName} already exist.\""
        kotlin.test.assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create employee should throw error given invalid employeeType`(){
        val employee = EntityGenerator.createEmployee().copy(
            employeeType = "Invalid UserType"
        )
        val exception = assertFailsWith<IllegalArgumentException> {
            employeeServiceImpl.createEmployee(employee)
        }
        val expectedException =
            "No enum constant com.example.simpletimetrackerapi.TimeTracker.utils.EmployeeType.Invalid UserType"
        kotlin.test.assertEquals(expectedException, exception.message)
    }

    @Test
    fun `update employee should return updated employee given valid inputs`(){
        val employee = EntityGenerator.createEmployee()
        val original = employeeRepository.save(employee)

        val body = original.copy(
            firstName = "Brenda",
            lastName = "Mage",
        )
        val updatedUser = employeeServiceImpl.updateEmployee(body, original.id!!)
        kotlin.test.assertEquals(body.firstName, updatedUser.firstName)
        kotlin.test.assertEquals(body.lastName, updatedUser.lastName)
    }
    @Test
    fun `update employee should failed if the userType changed`(){
        val employee = EntityGenerator.createEmployee()
        val original = employeeRepository.save(employee)

        val body = original.copy(
            employeeType = EmployeeType.Contractor.name
        )
        val exception = assertFailsWith<ResponseStatusException> {
            employeeServiceImpl.updateEmployee(body, original.id!!)
        }

        val expectedException =
            "409 CONFLICT \"Sorry, you are not allowed to change the employeeType.\""
        kotlin.test.assertEquals(expectedException, exception.message)
    }

    @Test
    fun `delete employee should change the isActive from true to false`(){
        val employee = EntityGenerator.createEmployee()
        val original = employeeRepository.save(employee)

        // To check if the default value of isActive is true
        kotlin.test.assertEquals(original.isActive, true)
        // To update the isActive value to false
        val checkEmployee = employeeServiceImpl.deleteEmployee(original.id!!)
        // To check if the isActive value is changed into false
        kotlin.test.assertEquals(checkEmployee.isActive, false)
    }
    @Test
    fun `delete employee should change the isActive from false to true`(){
        val employee = EntityGenerator.createEmployee()
        val original = employeeRepository.save(employee.copy(isActive = false))

        // To check if the default value of isActive is false
        kotlin.test.assertEquals(original.isActive, false)
        // To update the isActive value to true
        val checkEmployee1 = employeeServiceImpl.deleteEmployee(original.id!!)
        // To check if the isActive value is changed into true
        kotlin.test.assertEquals(checkEmployee1.isActive, true)
    }
    @Test
    fun `get employee should throw an error given invalid id`(){
        val invalidId: Long = 123
        val exception = assertFailsWith<ResponseStatusException> {
            employeeServiceImpl.getEmployeeById(invalidId)
        }

        val expectedException = "404 NOT_FOUND \"Employee with id: $invalidId does not exist.\""
        kotlin.test.assertEquals(expectedException, exception.message)

    }
    @Test
    fun `get All employee should return employees`(){
        val body = EntityGenerator.createEmployee()
        employeeRepository.saveAll(
            listOf(
                body,
                body.copy(firstName = "Christine",
                    lastName = "Alcazar")
            )
        )
        val employee = employeeServiceImpl.getAllEmployee()
        kotlin.test.assertEquals(2, employee.count())
    }
}