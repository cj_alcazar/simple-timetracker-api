package com.example.simpletimetrackerapi.employee

import com.example.simpletimetrackerapi.SimpleTimetrackerApiApplicationTests
import com.example.simpletimetrackerapi.TimeTracker.repositories.EmployeeRepository
import com.example.simpletimetrackerapi.TimeTracker.serviceImpl.EmployeeServiceImpl
import com.example.simpletimetrackerapi.utils.EntityGenerator
import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.*
@AutoConfigureMockMvc
class EmployeeServiceIntegrationTest: SimpleTimetrackerApiApplicationTests() {
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var objectMapper: ObjectMapper
    @Autowired
    private lateinit var employeeRepository: EmployeeRepository

    @Autowired
    private lateinit var employeeServiceImpl: EmployeeServiceImpl

    @BeforeEach
    fun setUp(){
        employeeRepository.deleteAll()
    }

    @Test
    fun `create employee should return 200`() {

        val body = EntityGenerator.createEmployee()

        mockMvc.post("/api/employee/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect { status { isOk() } }
    }


    @Test
    fun `create employee should return 409 if duplicate firstname and lastname`() {
        val user = employeeRepository.save( EntityGenerator.createEmployee())
        mockMvc.post("/api/employee/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(user)

        }.andExpect {status { isConflict() }
        }
    }

    @Test
    fun `create employee should return 404 if employeeType is invalid`() {
        val employee = employeeRepository.save( EntityGenerator.createEmployee().copy(
            employeeType = "Invalid EmployeeType"
        ))

        mockMvc.post("/api/employee/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(employee)
        }.andExpect {status { isConflict() }
        }
    }

    @Test
    fun `get employee should return employee`() {
        val user = employeeRepository.save(EntityGenerator.createEmployee())
        mockMvc.get("/api/employee/${user.id!!}/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    fun `update employee should return 200`() {

        val employee = EntityGenerator.createEmployee()
        val savedUser = employeeRepository.save(employee)
        val body = employee.copy(
            firstName = "Brenda",
            lastName = "Mage",
        )
        mockMvc.put("/api/employee/${savedUser.id!!}/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {status { isOk() }
        }
    }
    @Test
    fun `delete user should return 200`() {
        val employee = EntityGenerator.createEmployee()
        val savedUser = employeeRepository.save(employee)

        mockMvc.put("/api/employeeChange/${savedUser.id!!}/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {status { isOk() }
        }
    }



}